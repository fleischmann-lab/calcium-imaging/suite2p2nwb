folders = ["Mouse#7", "Mouse#8", "Mouse#9", "Mouse#163", "Mouse#164"]

rule all:
    input:
        expand("../Data/DataSimon/Odor_Set_1/{folder}/{folder}.nwb", folder=folders)

rule intermediateSuite2p:
    input:
        expand("../Data/DataSimon/Odor_Set_1/{folder}/", folder=folders)
    conda:
        "environment.yml"
    log: "logs/suite2p.log"
    output:
        expand("../Data/DataSimon/Odor_Set_1/{folder}/suite2p/ophys.nwb", folder=folders)
    script:
        "src/intermediateSuite2p.py"

rule fullNWB:
    input:
        suite2p = rules.intermediateSuite2p.output,
        all_files = expand("../Data/DataSimon/Odor_Set_1/{folder}/", folder=folders)
    params:
        session_description="My awesome session description"
    conda:
        "environment.yml"
    log: "logs/nwb.log"
    output:
        expand("../Data/DataSimon/Odor_Set_1/{folder}/{folder}.nwb", folder=folders)
    script:
        "src/convert2NWB.py"
