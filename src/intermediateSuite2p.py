import os
from pathlib import Path

import numpy as np
from pynwb import NWBHDF5IO
from suite2p.io.nwb import save_nwb

for input_dir in snakemake.input:
    DATAPATH = Path(input_dir)

    # Suite2p wants its folder
    SUITE2P_PATH = DATAPATH.joinpath("suite2p")
    if not SUITE2P_PATH.exists():
        SUITE2P_PATH.mkdir()

    ops1 = []
    for folder in os.listdir(DATAPATH):
        folder_path = DATAPATH.joinpath(folder)
        if Path.is_dir(folder_path) and "Plane" in folder:
            a = np.load(folder_path.joinpath("iscell.npy"))
            print(f"number of ROI: {len(a)}")
            ops = np.load(folder_path.joinpath("ops.npy"), allow_pickle=True)
            ops = ops.item()
            ops["save_path"] = str(folder_path)
            ops["save_path0"] = str(DATAPATH)
            ops1.append(ops)

    save_nwb(ops1)

    nwb_path = SUITE2P_PATH.joinpath("ophys.nwb")
    with NWBHDF5IO(str(nwb_path), "r") as io:
        nwb = io.read()
        print(nwb)
