import os
from pathlib import Path
from calimag.NwbConverter import NwbConverter


for input_path, output_path, suite2p_path in zip(
    snakemake.input.all_files, snakemake.output, snakemake.input.suite2p
):
    input_path = Path(input_path)
    output_path = Path(output_path)
    print(input_path)
    print(output_path)

    folders = os.listdir(input_path)
    for item in folders:
        if ".xml" in item.lower():
            scope_file_path = input_path.joinpath(item)
        if ".txt" in item.lower():
            teensy_file_path = input_path.joinpath(item)

    # TODO: Should be variable in some way
    TwoPhotonSeries = [
        "Placeholder1",
        "Placeholder2",
        "Placeholder3",
    ]
    nwb2p = Path(suite2p_path)

    print(scope_file_path)
    print(teensy_file_path)
    print(nwb2p)
    print(snakemake.params.session_description)

    conv = NwbConverter(
        session_description=snakemake.params.session_description,
        microscope_filepath=scope_file_path,
        teensy_filepath=teensy_file_path,
        segmentation_fp=str(nwb2p),
        TwoPhotonSeriesUrlOrPath=TwoPhotonSeries,
    )
    nwb_out = conv.Convert2NWB(nwb_output_path=str(output_path))
