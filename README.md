
To create your environment from scratch:
```
conda env create -f environment.yml
```
Then activate your environment:
```
conda activate Suite2p2NWB
```

Add the new packages to the `environment.yml` file, and then update your environment with:
```
conda env update -f environment.yml
```

Add your new environment (kernel) in Jupyter:
```
python -m ipykernel install --user --name=Suite2p2NWB
```

To make the widgets work in JupyterLab:
```
jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyter-matplotlib @jupyterlab/debugger
```
